# Annexes-mie-2024

- **federated_data_distribution.pdf**: Distribution of Total Documents across Medical Poles for Federated Learning Dataset.
 
- **experiments_overview.pdf**: Figure showing an Overview of different experiments involved in our study.
 
 